#pragma once

#include "windows.h"
#include <string>
#include <vector>
#include <iostream>
#include <tuple>

#include "Logger.h"

using namespace std;

class Bundle
{
public:
	Bundle(Logger *l);
	~Bundle();
	int launchX64();
	int setArgs(wstring args);
	int dropResource();

private:
	vector<tuple<wstring, wstring, WORD>> listResources;
	HMODULE mod;
	wstring matchedName;
	wstring matchedType;
	Logger *l;
	wstring args;


	int getRessource(wstring ressourceType, LPVOID* firstByte, int* size);
	int extractResource(LPVOID firstByte, int size, wstring path);
	int isRessourcePresent(wstring ressourceType);
	int initResources();
	
};

BOOL EnumTypesFunc(HMODULE hModule, LPTSTR lpType, LONG lParam);
BOOL EnumNamesFunc(HMODULE hModule, LPCTSTR lpType, LPTSTR lpName, LONG lParam);
BOOL EnumLangsFunc(HMODULE hModule, LPCTSTR lpType, LPCTSTR lpName, WORD wLang, LONG lParam);