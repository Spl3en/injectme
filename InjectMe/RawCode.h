#pragma once

#include <string>
#include <windows.h>
#include <iostream>

#include "Logger.h"
#include "const.h"
#include "ShellCode.h"

using namespace std;
class RawCode
{
public:
	RawCode(int type, Logger *l);
	RawCode(Logger *l);

	char* genMessageBoxSC();

	//setter
	int setPath(wstring path);

	//getter
	DWORD getSize();
	char* getFirstByte();
	wstring getAbsolutePath();
	~RawCode();

private:
	int getContent();

	wstring path;
	char* data;
	char* testCode;
	char type;
	Logger *l;
	DWORD fileSize;
	ShellCode* sc;
};

