
#include <stdio.h>
#include <tchar.h>
#include <vector>
#include <windows.h>
#include <iostream>
#include <errno.h>

#include "Processes.h"
#include "Dll.h"
#include "Logger.h"
#include "Injector.h"
#include "Engine.h"
#include "Bundle.h"
#include "RawCode.h"
#include "Usage.h"
#include "const.h"

using namespace std;

int _tmain(int argc, TCHAR* argv[])
{

	int b_dll = 0;
	int b_process = 0;
	int b_RawCode = 0;
	int b_sample = 0;
	int b_listProcess = 0;
	int b_drop = 0;
	int b_method = INJECT_METHOD_CREATEREMOTETHREAD;
	int b_help = 0;
	int b_unload = 0;

	int logLevel = 2;
	wstring dllPath = L"";
	vector<int> pid;
	wstring process = L"";
	wstring codePathFile = L"";
	wstring method = L"";
	wstring dllToUnload = L"";
	
	Logger log(logLevel);
	Engine e;
	Dll dll(&log);
	RawCode* rc;
	Processes p(&log);
#ifdef X_86
	Bundle b(&log);
#endif
	Usage u;

	if (argc == 1) {
		u.print(argv[0]);
		return 1;
	}

	wstring args = L"";
	//get parameters
	for (int i = 1; i < argc; i++){
		args += L" " + wstring(argv[i]);
		//print processes list
		if (!wcscmp(argv[i], L"-l")){
			b_listProcess = true;
		}

		//set dll Path
		if (!wcscmp(argv[i], L"-d") && (i + 1 < argc)){
			dllPath = argv[i + 1];	
		}

		//set verbosity level
		if (!wcscmp(argv[i], L"-v") && (i + 1 < argc)){
			logLevel = (int)e.w2i(argv[i+1]);
		}

		//set target with PID or processName 
		if (!wcscmp(argv[i], L"-p") && (i + 1 < argc)){
			process = wstring(argv[i + 1]);
		}

		//set path file containing raw code 
		if (!wcscmp(argv[i], L"-r") && (i + 1 < argc)) {
			codePathFile = wstring(argv[i + 1]);
		}

		//set path file containing raw code 
		if (!wcscmp(argv[i], L"-rs")) {
			b_sample = 1;
		}

		//set injection method
		if (!wcscmp(argv[i], L"-m") && (i + 1 < argc)) {
			method = wstring(argv[i + 1]);
		}

		//set injection method
		if (!wcscmp(argv[i], L"-h") || !wcscmp(argv[i], L"?")) {
			b_help = 1;
		}

		//set dll name to unload
		if (!wcscmp(argv[i], L"-u") && (i + 1 < argc)) {
			dllToUnload = wstring(argv[i + 1]);
			b_unload = 1;
		}

#ifdef X_86
		//set path file containing raw code 
		if (!wcscmp(argv[i], L"-D")) {
			b_drop = 1;
		}
#endif
	}

	//save args for x64 injector
#ifdef X_86
	b.setArgs(args);
#endif

	//set log level
	if (logLevel >= 0 && logLevel <= 3){
		log.setLevel(logLevel);
	}

	//help parameter
	if (b_help) {
		u.print(argv[0]);
		return 1;
	}

	//drop parameter
#ifdef X_86
	if (b_drop) {
		if (!b.dropResource()) {
			log.error(L"Can't drop injector64");
			return 0;
		}
		else {
			log.success(L"injector dropped");
			return 1;
		}
	}
#endif
	//init Objects
	p.initList();

	//print processes and exit
	if (b_listProcess){
		p.printProcesses();
		return 1;
	}

	//set injection method
	if (method.compare(L"")) {

		//createRemoteThread method
		if (!method.compare(L"1")) {
			b_method = INJECT_METHOD_CREATEREMOTETHREAD;
		}
		else if (!method.compare(L"2")) {
			b_method = INJECT_METHOD_SUSPENDEDTHREAD;
		}
		else if (!method.compare(L"3")) {
			b_method = INJECT_METHOD_NTQUEUEAPCTHREAD;
		}
		else {
			log.error(L"Injection method unknown, exiting...");
			return 0;
		}
	}

	//set Dll path
	if (dllPath.compare(L"")){
		if (dll.setPath(dllPath) == 1){
			dllPath = L"\"" + dllPath + L"\"";
			b_dll = 1;
		}
		else{
			log.error(L"Dll not found or invalid");
			return 0;
		}
	}

	//looking for targeted process
	if (process.compare(L"")){
		int pidArg = (int)e.w2i(process);

		//ProcessName case
		if (pidArg == 0){
			pid = p.getPid(process);
			if (pid.size() > 0){
				b_process = 1;
			}

		}
		//PID case
		else{
			process = p.getName(pidArg);
			if (process.compare(L"")){
				pid.push_back(pidArg);
				b_process = 1;
			}
		}
		//process defined but not found 
		if (!b_process){
			log.error(L"Process not found");
		}
	}

	//Check if file containing rawCode can be opened
	if (codePathFile.compare(L"")) {
		rc = new RawCode(SC_LOADDLL, &log);
		if (rc->setPath(codePathFile) == 1) {
			b_RawCode = 1;
		}
		else {
			log.error(L"RawCode file not found or invalid");
			return 0;
		}
	}

	// - need a DllName or DLLPath or a raw code or a raw code sample but not in the same time
	if (b_unload + b_dll + b_RawCode + b_sample > 1){
		log.error(L"Ambigious parameter...");
		u.print(argv[0]);
		return 0;
	}

	//for an unload action, a process and a dllName must be set
	if (b_unload &&  !b_process) {
		log.error(L"Need a remote process to unload targeted Dll");
		u.print(argv[0]);
		return 0;
	}

#if X_64
	int injectorType = X64;
#elif X_86
	int injectorType = X86;
#endif

	log.info(L"Injector: " + e.i2w(injectorType) + L" bits");

	//print information about targeted process(es) 
	if (b_process){
		log.info(L"--------------------------------");
		log.info(L"Process: " + process);

		for (auto it = pid.cbegin(); it != pid.cend(); ++it){
			log.info(L"Pid: " + e.i2w(*it) + L" (" + e.dec2hex(e.i2w(*it)) + L") / " + e.i2w(p.getType(*it)) + L" bits");
		}
	}

	//print information concerning DLL 
	if (b_dll){
		log.info(L"--------------------------------");
		log.info(L"Dll: " + dll.getPath());
		log.info(L"Dll architecture: " + to_wstring(dll.getType())+L" bits");
	}

	//print information concerning raw code file
	else if (b_RawCode) {
		log.info(L"--------------------------------");
		log.info(L"Raw code file: " + rc->getAbsolutePath());
		log.info(L"Size of raw code: " + to_wstring(rc->getSize()));
	}

	//print information concerning raw code sample
	else if (b_sample) {
		log.info(L"--------------------------------");
		log.info(L"Sample choosen: MessageBox");
	}
	//print information concerning remote Dll to unload
	else if (b_unload) {
		log.info(L"--------------------------------");
		log.info(L"Targeted dll: "+ dllToUnload);
		for (auto it = pid.cbegin(); it != pid.cend(); ++it) {
			if (p.isDllInProcess(*it, dllToUnload)) {
				log.info(e.i2w(*it) + L": Dll found ;)");
			}
			else {
				log.info(e.i2w(*it) + L": Dll not found :/");
			}
		}
	}
	//print error 
	else{
		log.info(L"Dll or RawCode not defined");
		return 0;
	}

	//print information concerning injection method
	if (b_method) {
		Const c;
		log.info(L"--------------------------------");
		log.info(L"Injection method: "+c.s_method[b_method]+ L" ("+to_wstring(b_method)+L")");
	}
	log.info(L"--------------------------------");

	//inject dll in remote process(es)
	if ((b_dll || b_unload || b_sample) && b_process){
		Injector ij(&log);
		for (auto it = pid.cbegin(); it != pid.cend(); ++it){

			//check if process can be opened
			if (p.getType(*it) == ERROR) {
				log.error(L"Process " + e.i2w(*it) + L" can't be injected");
				continue;
			}

			//check if remote process architecture == dll architecture
			if (b_dll && (dll.getType() != p.getType(*it) && dll.getType() != ERROR)) {
				log.error(L"Targeted process and Dll have differents architectures, need other DLL.");
			}

			//check if injector architecure == remote process architecture
			if (injectorType == p.getType(*it)) {

				//inject and execute Shellcode

				injector_struct is;
				if (b_dll) {
					is.dllPath = dll.getPath();
					if (!ij.InjectSC(b_method,SC_LOADDLL, *it, is)) {
						log.error(L"Injection in process " + to_wstring(*it) + L" Failed");
					}
				}
				if (b_unload) {
					is.dllModule = p.isDllInProcess(*it, dllToUnload);
					if (!ij.InjectSC(b_method, SC_UNLOADDLL, *it, is)) {
						log.error(L"Injection in process " + to_wstring(*it) + L" Failed");
					}
				}

				if (b_sample) {
					if (!ij.InjectSC(b_method, SC_MSGBOX, *it, is)) {
						log.error(L"Injection in process " + to_wstring(*it) + L" Failed");
					}
				}
					
			}

			//if injector is 32bits, drop 64 version and execute it
			else if(injectorType == X86){
#ifdef X_86
				log.info(L"Drop injector64");
				b.launchX64();
				return 1;
#endif	
			}
			else {
				log.error(L"Need x86 injector");
			}
		}		
	}

	//inject raw code in remote process(es)
	if ((b_RawCode) && b_process) {
		Injector ij(&log);
		for (auto it = pid.cbegin(); it != pid.cend(); ++it) {

			//check if process can be opened
			if (p.getType(*it) == ERROR) {
				log.error(L"Process " + e.i2w(*it) + L" can't be injected");
				continue;
			}

			//check if injector architecure == remote process architecture
			if (injectorType == p.getType(*it)) {

				//inject and execute Raw Code
				if (!ij.InjectRawCode(*it, rc->getFirstByte(), rc->getSize())) {
					log.error(L"Injection in process " + to_wstring(*it) + L" Failed");
				}
			}

			//if injector is 32bits, drop 64 version and execute it
			else if (injectorType == X86) {
#ifdef X_86
				log.info(L"Drop injector64");
				b.launchX64();
				return 1;
#endif	
			}
		}
	}

	return 1;
}