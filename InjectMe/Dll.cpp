#include "Dll.h"


Dll::Dll(TCHAR* path,Logger* log){
	setPath(path);
	this->l = log;
}

Dll::Dll(Logger* log){
	this->l = log;
	this->type = UNKNOWN;
}

Dll::~Dll(){
	
}

int Dll::setPath(wstring path){

	if (path.empty()) {
		return 0;
	}

	DWORD size = GetFullPathNameW(path.c_str(), 0, NULL, NULL);
	if (!size) {
		l->error(L"GetFullPathNameW");
		return 0;
	}

	TCHAR* fullPath = (TCHAR*)malloc((size + 1)*sizeof(TCHAR));
	if (!fullPath){
		l->error(L"malloc");
		return 0;
	}

	if (!GetFullPathNameW(path.c_str(), size, fullPath, NULL)){
		l->error(L"GetFullPathNameW");
		return 0;
	}
	this->path = wstring(fullPath);
	free(fullPath);
	type = _getType();
	if ( type == 0){
		return 0;
	}
	return 1;
}

wstring Dll::getPath(){
	return path;
}

int Dll::getType(){
	return type;
}

int Dll::_getType(){
	
	char buff[0x200] = { 0 };
	PVOID old = NULL;

	//disable FS redirection in system32 folder
	Wow64DisableWow64FsRedirection(&old);
	HANDLE hFile = CreateFile(path.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL,NULL);
	//enable FS redirection in system32 folder
	Wow64RevertWow64FsRedirection(old);

	if (hFile == INVALID_HANDLE_VALUE){
		if (GetLastError() == ERROR_FILE_NOT_FOUND){
			l->error(L"Not Found: "+path);
			return 0;
		}
		if (GetLastError() == ERROR_ACCESS_DENIED){
			l->error(L" Access denied: "+path);
			return 0;
		}
		
		l->error(L"CreateFile('" + path + L"')");
		return 0;
	}
	DWORD read = 0;
	if (!ReadFile(hFile, buff, 0x200, &read, NULL)){
		l->error(L"ReadFile('" + path + L"')");
		return 0;
	}

	CloseHandle(hFile);
	unsigned int offsetNTHeader = *(int*)(buff + 0x3c);
	if (offsetNTHeader > 0x150){
		l->error(L"Not a valid PE file");
		return 0;
	}

	DWORD sign = *(DWORD*)(buff + offsetNTHeader);
	if (sign != 0x4550){
		l->error(L"Not a valid PE file - can't find PE signature");
		return 0;
	}
	WORD magic = *(WORD*)(buff + offsetNTHeader + 0x18);

	if (magic == 0x010b){
		return X86;
	}
	else if (magic == 0x020b){
		return X64;
	}
	else{
		l->error(L"Unknown PE type");
		return 0;
	}

}