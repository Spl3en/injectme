#include "Bundle.h"


Bundle::Bundle(Logger* l){
	this->l = l;
	this->args = L"";
#ifdef X_86
	initResources();
#endif
}


Bundle::~Bundle(){
}

int Bundle::setArgs(wstring args){
	this->args = args;
	return 1;
}

int Bundle::isRessourcePresent(wstring ressourceType){

	int count = 0;
	for (vector<tuple<wstring, wstring, WORD>>::iterator it = listResources.begin(); it != listResources.end(); ++it){
		wstring name = get<0>(*it);
		wstring type = get<1>(*it);
		count++;
		if (!ressourceType.compare(type)){
			matchedName = name;
			matchedType = type;
			return 1;
		}
	}
	return 0;
}

int Bundle::getRessource(wstring ressourceType, LPVOID* firstByte, int* size) {
	if (!isRessourcePresent(ressourceType)){
		l->error(L"Resource not found");
		return 0;
	}

	HRSRC hRessource = FindResource(mod, MAKEINTRESOURCE(_wtoi(matchedName.c_str())), matchedType.c_str());
	int err = GetLastError();
	if (!hRessource){
		l->error(L"FindResource");
		return 0;
	}

	HGLOBAL hRessource2 = LoadResource(mod, hRessource);
	if (!hRessource2){
		l->error(L"LoadResource");
		return 0;
	}

	*firstByte = LockResource(hRessource2);
	if (!*firstByte){
		l->error(L"LockResource");
		return 0;
	}
	*size = SizeofResource(mod, hRessource);
	if (!*size){
		l->error(L"SizeofResource");
		return 0;
	}

	return 1;

}

int Bundle::initResources(){

	TCHAR currentFile[1000];
	if (GetModuleFileName(NULL, currentFile, 1000) >= 1000 || GetLastError() == ERROR_INSUFFICIENT_BUFFER){
		l->error(L"GetModuleFileName");
		return 0;
	}

	listResources.clear();
	mod = LoadLibraryEx(currentFile, NULL, DONT_RESOLVE_DLL_REFERENCES);
	if (!mod){
		l->error(L"LoadLibraryEx");
		return 0;
	}
	EnumResourceTypes(mod, (ENUMRESTYPEPROC)EnumTypesFunc, (LONG_PTR)&listResources);
	return (int)listResources.size();
}

int Bundle::extractResource(LPVOID firstByte, int size, wstring path){

	char* buffTmp = (char*)malloc(size);
	memcpy(buffTmp, (char*)firstByte, size);

	HANDLE hFile = CreateFile(path.c_str(), GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	if (!hFile){
		l->error(L"Can't create injector file");
		return 0;
	}
	DWORD written = 0;

	if (!WriteFile(hFile, buffTmp, size, &written, NULL)){
		l->error(L"Can't write in injector file");
		free(buffTmp);
		return 0;
	} 
	free(buffTmp);
	CloseHandle(hFile);
	return 1;
}

int Bundle::dropResource() {
	LPVOID firstByte = NULL;
	int size = 0;
	if (!getRessource(L"INJECTOR64", &firstByte, &size)) {
		l->error(L"Can't find INJECTOR64 resource");
		return 0;
	}
	if (!extractResource(firstByte, size, L"inject64.exe")) {
		l->error(L"Can't extract injector64");
		return 0;
	}
	return 1;
}

int Bundle::launchX64(){
	if (!dropResource()) {
		return 0;
	}

	//SI process
	STARTUPINFO startupInfo = { 0 };
	ZeroMemory(&startupInfo, sizeof(startupInfo));
	startupInfo.cb = sizeof(startupInfo);
	PROCESS_INFORMATION pi = { 0 };

	wstring cmd = L"inject64.exe"+args;
	l->info(L"\n=====> Launch x64 Injector <========\n");
	l->info(L"Command: "+cmd);
	BOOL err = CreateProcess(NULL,(LPWSTR)cmd.c_str(), NULL, NULL,TRUE,0, NULL, NULL, &startupInfo, &pi);
	
	WaitForSingleObject(pi.hProcess, INFINITE);
	CloseHandle(pi.hThread);
	CloseHandle(pi.hProcess);
	return 1;
}

/********************************/
/* Macros to get all ressources */
/********************************/

BOOL EnumTypesFunc(HMODULE hModule, LPTSTR lpType, LONG lParam){
	return EnumResourceNames(hModule, lpType, (ENUMRESNAMEPROC)EnumNamesFunc, lParam);
}

BOOL EnumNamesFunc(HMODULE hModule, LPCTSTR lpType, LPTSTR lpName, LONG lParam){
	return EnumResourceLanguages(hModule, lpType, lpName, (ENUMRESLANGPROC)EnumLangsFunc, lParam);
}

BOOL EnumLangsFunc(HMODULE hModule, LPCTSTR lpType, LPCTSTR lpName, WORD wLang, LONG lParam){
	wstring sName = L"";
	wstring sType = L"";

	if (IS_INTRESOURCE(lpType)){
		sType = to_wstring((int)lpType);
	}
	else{
		sType = wstring(lpType);
	}

	if (IS_INTRESOURCE(lpName)){
		sName = to_wstring((int)lpName);
	}
	else{
		sName = wstring(lpName);
	}

	vector<tuple<wstring, wstring, WORD>> *list = (vector<tuple<wstring, wstring, WORD>> *)lParam;
	tuple<wstring, wstring, WORD> t(sName, sType, wLang);
	list->push_back(t);
	return true;
}