#pragma once

#include "windows.h"
#include <string>
#include <iostream>
#include <math.h>

#include "const.h"
#include "Engine.h"

using namespace std;

class Logger
{
public:
	Logger();
	~Logger();

	Logger(int level);
	void setLevel(int level);
	int getLevel();

	void error(wstring log);
	void success(wstring log);
	void info(wstring log);
	void debug(wstring log);

private:
	int level;
	Engine e;
};

