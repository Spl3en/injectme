#pragma once

#include <map>
#include <string>
#include <vector>
#include "windows.h"
#include <tlhelp32.h>
#include <iostream>
#include <Psapi.h>

#include "Logger.h"
#include "const.h"
#include "Engine.h"

using namespace std;

class Processes
{
public:
	Processes(Logger* log);
	~Processes();

	void printProcesses();
	vector<int> getPid(wstring processName);
	wstring getName(int pid);
	int getType(int pid);
	void initList();
	int isInjectable(int pid);
	HMODULE isDllInProcess(int pid, wstring dll);

private:
	vector<tuple<int, wstring, int, int>> listProcess;
	Logger *l;
	Engine e;

	

};