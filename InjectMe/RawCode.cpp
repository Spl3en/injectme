#include "RawCode.h"



RawCode::RawCode(int type, Logger *l){
	this->l = l;
	this->type = type;
	path = L"";
	data = NULL;
	fileSize = 0;
}

RawCode::RawCode(Logger *l) {
	this->l = l;
	path = L"";
	data = NULL;
	fileSize = 0;
}

RawCode::~RawCode(){
	delete(sc);
}

int RawCode::setPath(wstring path) {
	DWORD size = GetFullPathNameW(path.c_str(), 0, NULL, NULL);
	if (!size) {
		l->error(L"GetFullPathNameW");
		return 0;
	}

	TCHAR* fullPath = (TCHAR*)malloc((size + 1)*sizeof(TCHAR));
	if (!fullPath) {
		l->error(L"malloc");
		return 0;
	}

	if (!GetFullPathNameW(path.c_str(), size, fullPath, NULL)) {
		l->error(L"GetFullPathNameW");
		return 0;
	}

	this->path = wstring(fullPath);
	free(fullPath);

	return getContent();
}

int RawCode::getContent() {
	//Open file
	HANDLE hFile = CreateFile(path.c_str(), GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if (!hFile) {
		l->error(L"CreateFile");
		return 0;
	}

	//Get the size of raw code
	DWORD highSize = 0;
	fileSize = GetFileSize(hFile, &highSize);

	if (highSize != 0) {
		l->error(L"File too big");
		return 0;
	}

	data = (char*)malloc(fileSize * sizeof(char));
	if (!data) {
		l->error(L"malloc");
		return 0;
	}

	//Get file content
	DWORD read = 0;
	if (!ReadFile(hFile, data, fileSize, &read, NULL)) {
		l->error(L"ReadFile");
		return 0;
	}

	//Close file
	CloseHandle(hFile);
	return 1;
}

char* RawCode::genMessageBoxSC() {
	sc = new ShellCode(SC_MSGBOX, NULL, l);
	this->fileSize = sc->getSize();
	this->data = sc->getShellCode();
	return data;
}

DWORD RawCode::getSize() {
	return fileSize;
}

char* RawCode::getFirstByte() {
	return data;
}

wstring RawCode::getAbsolutePath() {
	return path;
}