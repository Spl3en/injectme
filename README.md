Last standalone executable is accessible [here](https://bitbucket.org/C4t0ps1s/injectme/raw/fbfbc7df6f8ffc5d4b090617282fe5daca49a4ed/Standalone/injectme.exe)

InjectMe
========
It's a tool to inject a specific Dll or raw code into a targeted process.  
A shellcode header is added before the call to the LoadLibrary API in the remote process.  
This code takes the return value of LoadLibrary, GetLastError and send it in the injector.  
With this tool, it's possible to know if the injection is a success or why it has failed.  
Like with a normal LoadLibrary, it's possible to unload a DLL and get the return code of the FreeLibrary API.  

### Example ###

The goal in this example is to inject a Dll in every Chrome children' processes.
Injection doesn't work because children are sandboxed and they can't call the LoadLibrary API.

*injectme.exe -p chrome.exe -d DllSample.dll*
```
[Info] Injector: 32 bits
[Info] --------------------------------
[Info] Process: chrome.exe
[Info] Pid: 2640 (0xa50) / 32 bits
[Info] Pid: 4760 (0x1298) / 32 bits
[Info] Pid: 2340 (0x924) / 32 bits
[Info] Pid: 4360 (0x1108) / 32 bits
[Info] Pid: 4112 (0x1010) / 32 bits
[Info] --------------------------------
[Info] Dll: C:\Users\C4t0ps1s\Desktop\DllSample.dll
[Info] Dll architecture: 32 bits
[Info] --------------------------------
[Info] Injection method: CreateRemoteThread (1)
[Info] --------------------------------
[Info] Shared address: 0x080A0000
[Info] Injected code address: 0x080B0000
[Info] Starting monitoring...
[Success] [0xa50] Success :) Dll injected into address 0x0x6D9B0000
[Info] Shared address: 0x05F40000
[Info] Injected code address: 0x05F50000
[Info] Starting monitoring...
[Error 0x00000057] PID 0x1298: remote call error = 0x00000005
[Info] Shared address: 0x03C60000
[Info] Injected code address: 0x03C70000
[Info] Starting monitoring...
[Error 0x00000057] PID 0x924: remote call error = 0x00000005
[Info] Shared address: 0x03BD0000
[Info] Injected code address: 0x05000000
[Info] Starting monitoring...
[Error 0x00000057] PID 0x1108: remote call error = 0x00000005
[Info] Shared address: 0x03D90000
[Info] Injected code address: 0x03DA0000
[Info] Starting monitoring...
[Error 0x00000057] PID 0x1010: remote call error = 0x00000005
```

* Dll is injected in parent process(2640) at memory address 0x6D9B0000.
* Injection in children processes fails with a "Access Denied" error (more details [here](https://msdn.microsoft.com/en-us/library/windows/desktop/ms681382(v=vs.85).aspx)).

## Options ##
```
Usage: injectme.exe <Source> <Destination> [Options]

SOURCE:
    -d <Dllpath>     injected Dll path
    -u <Dllname>     Unload targeted DLL in remote process
    -r <RawCodepath> injected raw shellcode path
    -rs              raw shellcode sample (messageBox)

DESTINATION:
    -p <PID or name> targeted process

OPTIONS:
    -v <level>       verbosity level: [0..3]
    -m <method>      injection method: [1..2]
        1 - CreateRemoteThread (default)
        2 - SuspendThread

MISC:
    -l               show processes and exit
    -D               Drop x64 version of injector in current folder and exit
    -h | ?           Print this help and exit
```

**List processes on current system:**

It returns PID | Architecture | Injectable | ProcessName.  
NB: if Archi is 0, it means that the process can't be opened with the OpenProcess API.

*injectme.exe -l*
```
      PID    Archi Injectable  Image
---------------------------------------------------------------
        0       0       No      [System Process]
        4       0       No      System
        268     0       No      smss.exe
        372     0       No      csrss.exe
        444     0       No      csrss.exe
        464     0       No      wininit.exe
...
        1388    64      Yes     taskhostw.exe
        2508    64      Yes     sihost.exe
        2380    64      Yes     TPAutoConnect.exe
        2292    64      Yes     conhost.exe
        992     64      Yes     explorer.exe
        3372    0       No      svchost.exe
        2452    32      Yes     OneDrive.exe
...
```

**get information about a process:**

*injectme.exe -p explorer.exe*

```
[Info] Injector: 32 bits
[Info] --------------------------------
[Info] Process: explorer.exe
[Info] Pid: 992 (0x3e0) / 64 bits
```

Parameter p works with a PID or a process name: 

*injectme.exe -p 992*

```
[Info] Injector: 32 bits
[Info] --------------------------------
[Info] Process: explorer.exe
[Info] Pid: 992 (0x3e0) / 64 bits
```

**get information about a Dll:**


*injectme.exe -d test.dll*
```
[Info] Injector: 32 bits
[Info] --------------------------------
[Info] Dll: C:\Users\C4t0ps1s\Desktop\DllSample.dll
[Info] Dll architecture: 32 bits
[Info] --------------------------------
[Info] Injection method: CreateRemoteThread (1)
[Info] --------------------------------
```

**Injection Methods**

There are 2 methods available to inject code:  

* Method 1: with the CreateRemoteThread API (method used by default).  
    Code is written into the remote process and the code is executed by a new thread.
* Method 2: with suspendThread / resumeThread APIs.  
    Code is also written into the remote process, but one of existing thread is suspended in order to execute the injected code. Once the end of the injected code is reached, InjectMe sets the old context of the hijacked thread and the target proceeds its normal execution.   

**Dll injection in a targeted process (32bits) with method SuspendThread (method 2):**

*injectme.exe -d DllSample.dll -p firefox.exe -m 2*
```
[Info] Injector: 32 bits
[Info] --------------------------------
[Info] Process: firefox.exe
[Info] Pid: 2160 (0x870) / 32 bits
[Info] --------------------------------
[Info] Dll: C:\Users\C4t0ps1s\Desktop\DllSample.dll
[Info] Dll architecture: 32 bits
[Info] --------------------------------
[Info] Injection method: SuspendThread (2)
[Info] --------------------------------
[Info] Shared address: 0x16600000
[Info] Injected code address: 0x16610000
[Info] Starting monitoring...
[Success] [0x870] Success :) Dll injected into address 0x0x6D9B0000
```

**Dll injection in a targeted process (64bits) with method 1 (CreateRemoteThread):**

In this case, Dll and remote process are 64bits architecture.
Injector drops in current folder a 64 bits version of itself and launches it.

*injectme.exe -d test.64dll -p explorer.exe*

```
[Info] Injector: 32 bits
[Info] --------------------------------
[Info] Process: explorer.exe
[Info] Pid: 992 (0x3e0) / 64 bits
[Info] --------------------------------
[Info] Dll: C:\Users\C4t0ps1s\Desktop\DllSample64.dll
[Info] Dll architecture: 64 bits
[Info] --------------------------------
[Info] Injection method: CreateRemoteThread (1)
[Info] --------------------------------
[Info] Drop injector64
[Info]
=====> Launch x64 Injector <========

[Info] Command: inject64.exe -d DllSample64.dll -p explorer.exe
[Info] Injector: 64 bits
[Info] --------------------------------
[Info] Process: explorer.exe
[Info] Pid: 992 (0x3e0) / 64 bits
[Info] --------------------------------
[Info] Dll: C:\Users\C4t0ps1s\Desktop\DllSample64.dll
[Info] Dll architecture: 64 bits
[Info] --------------------------------
[Info] Injection method: CreateRemoteThread (1)
[Info] --------------------------------
[Info] Shared address: 0x00000000078A0000
[Info] Injected code address: 0x00000000078B0000
[Info] Starting monitoring...
[Success] [0x3e0] Success :) Dll injected into address 0x0x00007FFAEA470000
```

**Unload a specific dll from a targeted process:**

*injectme.exe -u DllGrabber.dll -p firefox.exe*

```
[Info] Injector: 32 bits
[Info] --------------------------------
[Info] Process: firefox.exe
[Info] Pid: 3000 (0xbb8) / 32 bits
[Info] --------------------------------
[Info] Targeted dll: DllGrabber.dll
[Info] 3000: Dll found
[Info] --------------------------------
[Info] Injection method: CreateRemoteThread (1)
[Info] --------------------------------
[Info] Shared address: 0x0D2D0000
[Info] Injected code address: 0x0D2E0000
[Info] Starting monitoring
[Success] [0xbb8] Success :) Dll unloaded
```

**Execute assembler code from a file:**

*injectme.exe -r RawCode.asm -p firefox.exe*

```
[Info] Injector: 32 bits
[Info] --------------------------------
[Info] Process: firefox.exe
[Info] Pid: 2160 (0x870) / 32 bits
[Info] --------------------------------
[Info] Raw code file: C:\Users\C4t0ps1s\Desktop\rawCode.asm
[Info] Size of raw code: 30
[Info] --------------------------------
[Info] Injection method: CreateRemoteThread (1)
[Info] --------------------------------
[Info] Injected code address: 0x00810000
```

**Drop x64 injector into current folder**

*injectme.exe -D*

```
[Success] injector dropped
```